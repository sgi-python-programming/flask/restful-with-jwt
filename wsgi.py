from app import app


if __name__ == "__main__":
    # THIS IS INTENDED FOR PRODUCTION ENV USING GUNICORN
    # e.g: gunicorn --bind 0.0.0.0:8000 wsgi:app
    #   or gunicorn -w 4 --bind 0.0.0.0:8000 wsgi:app
    app.run(host="0.0.0.0", debug=False)
