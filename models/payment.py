from db import db


class PaymentModel(db.Model):
    __tablename__ = 'payments'

    id = db.Column(db.Integer, primary_key=True)
    payment_date = db.Column(db.Date)
    amount = db.Column(db.String(50))

    def json(self):
        return {'id': self.id,
                'payment_date': self.payment_date,
                'amount': self.amount}

    def print_self(self):
        print(self)

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()