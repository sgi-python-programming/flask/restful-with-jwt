from os import getenv
from dotenv import load_dotenv


load_dotenv()
postgresql = { 'host': getenv('DB_HOST'),
               'user': getenv('DB_USER'),
               'passwd': getenv('DB_PASS'),
               'db': getenv('DB_NAME')
               }

postgresqlConfig = "postgresql+psycopg2://{}:{}@{}/{}".format(postgresql['user'], postgresql['passwd'], postgresql['host'], postgresql['db'])

