from flask import Flask
from flask_jwt_extended import JWTManager
from flask_restful import Api
from resources.item import Item, ItemList
from resources.store import Store, StoreList
from resources.user import UserRegister, User
from config import postgresqlConfig

app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = postgresqlConfig
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
# Setup the Flask-JWT-Extended extension
app.config["JWT_SECRET_KEY"] = "Dese.Decent.Pups.BOOYO0OST"  # Change this!
jwt = JWTManager(app)
api = Api(app)


#@app.before_first_request
def create_tables():
    from db import db
    db.init_app(app)
    db.create_all()


# jwt = JWT(app, authenticate, identity)  # Auto Creates /auth endpoint

api.add_resource(Item, '/item/<string:name>')
api.add_resource(ItemList, '/items')
api.add_resource(UserRegister, '/register')
api.add_resource(User, '/user')
api.add_resource(Store, '/store/<string:name>')
api.add_resource(StoreList, '/stores')

with app.app_context():
    create_tables()


if __name__ == '__main__':
    print('THIS IS FOR DEVELOPMENT ENV ONLY!')
    app.run(host='127.0.0.1', port='5000', debug=True)
